% Copyright (c) 2016 Gergely Nagy
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
% documentation files (the "Software"), to deal in the Software without restriction, including without
% limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
% Software, and to permit persons to whom the Software is furnished to do so, subject to the following
% conditions:
% 
% The above copyright notice and this permission notice shall be included in all copies or substantial portions
% of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
% TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
% THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
% CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
% DEALINGS IN THE SOFTWARE.
\LoadClass[8pt]{extarticle}
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{cheatsheet}[2016/11/08 Cheat sheet document style]

\RequirePackage[paper=a4paper, top=0cm, left=.8cm, right=.8cm, bottom=1cm, includeheadfoot, landscape]{geometry}
\RequirePackage{fancyhdr} % must be loaded after geometry!
\RequirePackage{multicol}
\RequirePackage[utf8]{inputenc}
\RequirePackage[magyar]{babel}
\RequirePackage{graphicx}
\RequirePackage[pdfborder={0 0 0}]{hyperref}
\RequirePackage[T1]{fontenc}
\RequirePackage{lmodern}
\RequirePackage{hfoldsty} % old style numbers
\RequirePackage{listings} % for better code listing
\RequirePackage{bold-extra} % needed by listings
\RequirePackage{color}
\RequirePackage{enumitem}
\RequirePackage[framemethod=tikz]{mdframed}
\RequirePackage{amssymb}
\usetikzlibrary{shadows}

\hypersetup{colorlinks, breaklinks, urlcolor=black, linkcolor=black}

%\usepackage{caption}
%\captionsetup[figure]{labelfont={bf,it},textfont=it}
%\captionsetup[table]{labelfont={bf,it},textfont=it}
%
%\usepackage{wrapfig}

\setlength{\parindent}{2pt}
\setlength{\columnseprule}{0.5pt}
\setlength{\columnsep}{10pt}
\setlist[itemize]{noitemsep, topsep=2pt}
\setlist[enumerate]{noitemsep, topsep=2pt}
\renewcommand{\labelitemi}{\tiny$\blacksquare$} 

\pagestyle{fancy}
\fancyhf{}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0.3pt}
\fancyhf[FL]{\footnotesize{
	\begin{flushleft}
		\emph{C++11 referencia}
	\end{flushleft}
}}
\fancyhf[FC]{
	\begin{center}
		\emph{\thepage}
	\end{center}
}
\fancyhf[FR]{\footnotesize{
	\begin{flushright}
		\emph{\today}
	\end{flushright}
}}

\lstset{basicstyle=\small\ttfamily, stringstyle=\ttfamily,
keywordstyle=\bfseries\ttfamily,aboveskip=2pt,
belowskip=2pt, tabsize=2, xleftmargin=5pt, captionpos=b, language=C++,
literate={á}{{\'a}}1 {é}{{\'e}}1 {ó}{{\'o}}1 {ö}{{\"o}}1 {ú}{{\'u}}1 {ü}{{\"u}}1 {í}{{\'i}}1 {ő}{{\H o}}1
{Đ}{{\"d}}1}

\newcommand{\idez}[1]{,,#1''}
\newcommand{\todo}[1]{\textit{\textbf{\textcolor{red}{Todo~}}}$[$\textit{#1}$]$}

\definecolor{ultralightgray}{RGB}{240,240,240}
\newcommand{\cppstd}[1]{\fcolorbox{white}{ultralightgray}{\textbf{\textit{\footnotesize{C++#1}}}}}

\newmdenv[rightline=true,leftline=true,innerleftmargin=0pt,roundcorner=4pt,shadow=true,shadowsize=4pt,shadowcolor=black!95]{sectionbox}
\newcommand{\cppsection}[2]{
	\begin{sectionbox}\vspace{-1.5em}{\centering\section*{#1}\label{sec:#2}\addcontentsline{toc}{section}{#1}\vspace{-1.2em}}\end{sectionbox}
}

\newcommand{\cppsubsection}[2]{
	\vspace{-1em}\subsection*{#1}\label{subsec:#2}\addcontentsline{toc}{subsection}{#1}\vspace{-.5em}
}
\lstnewenvironment{cpplist}{}{}
\newmdenv[topline=false,rightline=false,bottomline=false]{hintbox}
\newenvironment{hint}
	{
		\begin{center}
			\begin{minipage}[h]{0.95\columnwidth}
				\small\itshape
				\begin{hintbox}
	}
	{
				\end{hintbox}
			\end{minipage}
		\end{center}
	}
